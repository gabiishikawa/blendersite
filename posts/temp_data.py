post_data = [{
    "id": "1",
    "title": "Blue Room",
    "date": "08/01",
    "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    "image_url": "https://imgur.com/6Gg3ZgA.jpg",

}, {
    "id": "2",
    "title": "Blue Kitchen",
    "date": "12/02",
    "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    "image_url": "https://imgur.com/LNzpUD4.jpg",

}, {
    "id": "3",
    "title": "Pink Room",
    "date": "21/03",
    "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    "image_url": "https://imgur.com/U7fl8NV.jpg",

}, {
    "id": "4",
    "title": "Green Room",
    "date": "09/04",
    "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    "image_url": "https://imgur.com/4joAsov.jpg",

}, {
    "id": "5",
    "title": "Orange Room",
    "date": "19/05",
    "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    "image_url": "https://imgur.com/H1TmnCm.jpg",

}, {
    "id": "6",
    "title": "Yellow Duplex",
    "date": "02/06",
    "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    "image_url": "https://imgur.com/Cwmfj8a.jpg",

}, {
    "id": "7",
    "title": "Purple Room",
    "date": "28/07",
    "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    "image_url": "https://imgur.com/mdNwW8a.jpg",

}, {
    "id": "8",
    "title": "Green Bathroom",
    "date": "15/08",
    "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    "image_url": "https://imgur.com/wuQy3jV.jpg",

}, {
    "id": "9",
    "title": "White Bathroom",
    "date": "17/09",
    "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    "image_url": "https://imgur.com/3fAfljD.jpg",

}]