from django.forms import ModelForm
from .models import Post, Comment
from django import forms

class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'title',
            'date',
            'image_url',
            'content'
        ]
        labels = {
            'title': 'Título',
            'date': 'Data de Postagem',
            'image_url': 'URL da Imagem',
            'content': 'Conteúdo'
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'date',
            'text',
        ]
        labels = {
            'author': 'Usuário',
            'date': 'Data',
            'text': 'Comentário',
        }