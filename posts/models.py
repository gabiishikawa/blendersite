from django.db import models
from django.conf import settings

class Post(models.Model):
    title = models.CharField(max_length=255)
    date = models.DateTimeField()
    image_url = models.URLField(max_length=200, null=True)
    content = models.TextField()

    def __str__(self):
        return f'{self.title} ({self.date})'

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    date = models.DateTimeField()
    text = models.CharField(max_length=255)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'


class Category(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    posts = models.ManyToManyField(Post)

    def __str__(self):
        return f'{self.name}'