from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import generic

from .temp_data import post_data
from .models import Post, Comment, Category
from .forms import PostForm, CommentForm

def detail_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    context = {'post': post}
    return render(request, 'posts/detail.html', context)


def detail_category(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    context = {'category': category}
    return render(request, 'posts/detailcategory.html', context)


def list_posts(request):
    post_list = Post.objects.all()
    context = {"post_list": post_list}
    return render(request, 'posts/index.html', context)


class PostListView(generic.ListView):
    model = Post
    template_name = 'posts/index.html'


class CategoryListView(generic.ListView):
    model = Category
    template_name = 'posts/categories.html'


def create_post(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post_title = form.cleaned_data['title']
            post_date = form.cleaned_data['date']
            post_image_url = form.cleaned_data['image_url']
            post_content = form.cleaned_data['content']
            post = Post(title=post_title,
                        date=post_date,
                        image_url=post_image_url,
                        content=post_content)
            post.save()
            return HttpResponseRedirect(
                reverse('posts:detail', args=(post.id, )))
    else:
        form = PostForm()
    context = {'form': form}
    return render(request, 'posts/create.html', context)


def update_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)

    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post.title = form.cleaned_data['title']
            post.date = form.cleaned_data['date']
            post.image_url = form.cleaned_data['image_url']
            post.content = form.cleaned_data['content']
            post.save()
            return HttpResponseRedirect(
                reverse('posts:detail', args=(post.id, )))
    else:
        form = PostForm(
            initial={
                'title': post.title,
                'date': post.date,
                'image_url': post.image_url,
                'content': post.content
            })

    context = {'post': post, 'form': form}
    return render(request, 'posts/update.html', context)


def delete_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)

    if request.method == "POST":
        post.delete()
        return HttpResponseRedirect(reverse('posts:index'))

    context = {'post': post}
    return render(request, 'posts/delete.html', context)


def create_comment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_date = form.cleaned_data['date']
            comment_text = form.cleaned_data['text']
            comment = Comment(author=comment_author,
                            date=comment_date,
                            text=comment_text,
                            post=post)
            comment.save()
            return HttpResponseRedirect(
                reverse('posts:detail', args=(post_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'post': post}
    return render(request, 'posts/comment.html', context)